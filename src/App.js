import React from 'react';
import { FooterComponent } from './components/layout/FooterComponent/FooterComponent';
import './App.css';
import {
  BrowserRouter as Router
} from 'react-router-dom';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab, faFacebook, faTwitter, faLinkedin, faInstagram, faSkype } from '@fortawesome/free-brands-svg-icons'
import RouterComponent from './components/router/RouterComponent';
import { HeaderComponent } from './components/layout/HeaderComponent/HeaderComponent';
library.add(fab, faFacebook, faTwitter, faLinkedin, faInstagram, faSkype )

function App(){
  return (
    <Router>
        <HeaderComponent />
        <RouterComponent />
        <FooterComponent />
    </Router>
  );
};

export default App;
