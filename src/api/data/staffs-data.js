export function createStaffData() {
    return [
        {
            image: 'https://www.usnews.com/dims4/USNEWS/ad4f1a8/2147483647/thumbnail/640x420/quality/85/?url=http%3A%2F%2Fmedia.beam.usnews.com%2Ffe%2F6b2988cd47d84ecae5217d4445381a%2Fjobs-job-photo-131.jpg',
            name: 'Yennefer',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
        },
        {
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcReSIqYYbhYV56cXluDzXCYe0-w9n6IgewROA&usqp=CAU',
            name: 'Hannah',
            description: 'Vitae proin sagittis nisl rhoncus mattis rhoncus urna neque. Enim lobortis scelerisque fermentum dui faucibus in ornare quam. Habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Cursus risus at ultrices mi tempus imperdiet nulla malesuada. Elementum curabitur vitae nunc sed velit dignissim. Morbi tincidunt augue interdum velit euismod in. Ullamcorper velit sed ullamcorper morbi.'
        },
        {
            image: 'https://media.istockphoto.com/photos/nail-filing-selective-focus-of-nail-salon-picture-id823559948?k=6&m=823559948&s=612x612&w=0&h=fW0-YMQmacxwHRii6kiWGYODC8Rqp7nb8TOISMTnwTY=',
            name: 'Ciri',
            description: 'Aliquet lectus proin nibh nisl condimentum id venenatis a condimentum. Id consectetur purus ut faucibus pulvinar elementum. Faucibus purus in massa tempor nec. Sit amet mauris commodo quis imperdiet massa. Eu facilisis sed odio morbi quis commodo odio aenean sed. Sit amet consectetur adipiscing elit. Est ultricies integer quis auctor. Auctor elit sed vulputate mi sit amet mauris.'
        }
    ];
};