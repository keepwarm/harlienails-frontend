import React from 'react';
import { Card, Button } from 'react-bootstrap';

export class StaffCardComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src= {this.props.image} />
                <Card.Body>
                    <Card.Title>{this.props.name}</Card.Title>
                    <Card.Text>
                        {this.props.description}.
                </Card.Text>
                    <Button variant="primary">See Detail</Button>
                </Card.Body>
            </Card>
        );
    }
}





