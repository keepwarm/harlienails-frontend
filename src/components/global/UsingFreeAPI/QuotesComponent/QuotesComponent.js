import React from 'react';
import axios from 'axios';

export class QuotesComponent extends React.Component {
    constructor(props){
        super(props);
        this.state = {quote: null};
    }

    componentDidMount(){
        console.log('QuotesComponent.componentDidMount()');
        axios.get(`http://api.forismatic.com/api/1.0/?method=getQuote&lang=en&&format=json`)
        .then(res => {
            console.log(res);
            const niceQuote = res.data;
            this.setState({quote: niceQuote});
        });
    }

    render(){
        return(
            <div>
                <h1>Nice Quotes</h1>
                {/* {this.state.quote &&
                    {this.state.niceQuote.}
                } */}
            </div>
        );
    }
};