import React from 'react';
import axios from 'axios';

export class AdvicesComponent extends React.Component{
    constructor(props){
        super(props);
        this.state = {advice: null};
    }

    componentDidMount(){
        axios.get(`https://api.adviceslip.com/advice`)
            .then(response => {
                console.log(response);
                this.setState({advice: response.data});
            });
    }

    render(){
        return(
            <div>
                <h1>Advices</h1>
                {this.state.advice && /* if this.state.weather is not null */
                    <h3>{this.state.advice.slip.advice}</h3>
                }
            </div>
        );
    }
};