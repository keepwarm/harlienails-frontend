import React from 'react';
import { Card } from 'react-bootstrap';
import './Coupons.css';

export class Coupons extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div className='coupon'>
                <div className='sub-coupon-1 coupon-rounded'>
                    <div>All services<br />10% discount with purchases over $50 </div>
                </div>

                <div className = 'sub-coupon-2 coupon-thumbnail'>
                    <div>Pedicure<br/> 10% discount for students with school ID </div>
                </div>

                <div className='sub-coupon-3 coupon-rounded'>
                    <div>Tuesday - Big Discount<br />10% discount on all services</div>
                </div>
            </div>
        );
    }
};