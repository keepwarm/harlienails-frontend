import React from 'react';
import {Card, Button} from 'react-bootstrap';

export class ServicesComponent extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
            <>
                <Card border="primary" style={{ width: '18rem' }}>
                    <Card.Header className = 'text-center text-uppercase font-weight-bold' >{this.props.header}</Card.Header>
                    <Card.Img variant="top" src={this.props.image} />
                    <Card.Body>
                        <Card.Title>{this.props.title}</Card.Title>
                        <Card.Text>
                            {this.props.content}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </>
        );
    }
};