import React from 'react';
import Swiper from 'react-id-swiper';

export class Gallery extends React.Component{
    params = {
        effect: 'coverflow',
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: 'auto',
        coverflowEffect: {
          rotate: 50,
          stretch: 0,
          depth: 100,
          modifier: 1,
          slideShadows: true
        },
        pagination: {
          el: '.swiper-pagination'
        }
      }
    render(){
        return (
            // <Swiper {...this.params}>
            //     <div class="swiper-slide" style={{backgroundImage:"url(media/gallery1.jpg)"}}></div>
            //     <div class="swiper-slide" style={{backgroundImage:"url(media/gallery2.jpg)"}}></div>
            //     <div class="swiper-slide" style={{backgroundImage:"url(media/gallery3.jpg)"}}></div>
            //     <div class="swiper-slide" style={{backgroundImage:"url(media/gallery4.jpg)"}}></div>
            //     <div class="swiper-slide" style={{backgroundImage:"url(media/gallery5.jpg)"}}></div>
            // </Swiper>
                <Swiper {...this.params}>
                  <div class="swiper-slide" style={{backgroundImage:"url(http://lorempixel.com/600/600/nature/1)"}}></div>
                  <div class="swiper-slide" style={{backgroundImage:"url(http://lorempixel.com/600/600/nature/1)"}}></div>
                  <div class="swiper-slide" style={{backgroundImage:"url(http://lorempixel.com/600/600/nature/1)"}}></div>
                  <div class="swiper-slide" style={{backgroundImage:"url(http://lorempixel.com/600/600/nature/1)"}}></div>
                  <div class="swiper-slide" style={{backgroundImage:"url(http://lorempixel.com/600/600/nature/1)"}}></div>
                </Swiper>
        );
    }
};
      