import React from 'react';
import './ServiceItem.css';

export default function ServiceItem(props) {
    const renderContent = () => {
        if(props.content){
            return <p className="content">{props.content}</p>
        }
        return null;
    }
    return (
        <div className="service-item">
            <h6>{props.title}<span className="price">{currency(props.price)}</span></h6>
            {renderContent()}
        </div>
    )
}

function currency(num){
    return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}