import React from 'react';
import { Container } from 'react-bootstrap';
import './PricingMenu.css';

export default function PricingMenu(props) {
    return (
        <Container>
            <div className="pricing-menu">
                <div className="outer-border">
                    <div className="mid-border">
                        <div className="inner-border" style={{backgroundImage:`url(${props.image})`}}>
                            <div className="content">
                                {props.children}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Container>
    )
}
