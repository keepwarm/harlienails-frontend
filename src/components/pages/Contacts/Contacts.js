import React from 'react';
import { Row, Container, Col, Form, Card, Image, Button } from 'react-bootstrap';
import { FormRow } from 'react-bootstrap/Form';
import logo from '../../../media/HarlieNailsLogo.png';
import './Contacts.css';

export default function Contacts() {
    return (
        <div className="contactPage">

            <Container>
                <Row>
                    <Col>
                        <h1 className="text-center">Contact Us</h1>
                    </Col>
                </Row>
            </Container>
            <section>
                <Container>
                    <Card style={{ border: '1px dashed' }}>
                        <Row noGutters={true}>
                            <Col>
                                <Card.Title className="text-center" style={{ fontSize: '2.5em' }}>
                                    Have Questions? <br />
                                    <small style={{ fontSize: '0.5em' }}>Feel Free To Get In Touch!</small>
                                </Card.Title>
                                <Card.Body className="contact">
                                    <Form>
                                        <Form.Row>
                                            <Form.Group as={Col} controlId='firstName'>
                                                <Form.Control type='text' placeholder='Your First Name' />
                                            </Form.Group>

                                            <Form.Group as={Col} controlId='lastName'>
                                                <Form.Control type='text' placeholder='Your Last Name' />
                                            </Form.Group>
                                        </Form.Row>

                                        <Form.Group controlId='email'>
                                            <Form.Control type='email' placeholder='name@example.com' />
                                        </Form.Group>

                                        <Form.Group controlId='subject'>
                                            <Form.Control type='text' placeholder='subject' />
                                        </Form.Group>

                                        <Form.Group controlId='description'>
                                            <Form.Label>Description</Form.Label>
                                            <Form.Control as='textarea' rows='3' />
                                        </Form.Group>
                                        <Form.Group>
                                            <Button className="w-100">Submit</Button>
                                            {/* TODO: //update with this: https://codepen.io/eyesight/pen/KGEebY */}
                                        </Form.Group>
                                    </Form>
                                </Card.Body>
                            </Col>
                            <Col>
                                {/* <Image fluid src="https://www.thoughtco.com/thmb/ETpeAugLo3JvjZbD1Mn3fLqII08=/1885x1414/smart/filters:no_upscale()/lotus-flower-828457262-5c6334b646e0fb0001dcd75a.jpg"></Image> */}
                                <img className="mx-auto d-block" src={logo} height='90' width='120' alt='Harlie Nails' />
                                <Card.Body className="contact">
                                    <Form>
                                        <Form.Row>
                                            <Form.Group as={Col}>
                                                <Form.Label style={{ fontWeight: 'bold' }}>Address:</Form.Label>
                                            </Form.Group>
                                            <Form.Group as={Col}>
                                                <Form.Label>555 NW No Name St. Johnston, Iowa 50151</Form.Label>
                                            </Form.Group>
                                        </Form.Row>
                                        <Form.Row>
                                            <Form.Group as={Col}>
                                                <Form.Label style={{ fontWeight: 'bold' }}>Email Address:</Form.Label>
                                            </Form.Group>
                                            <Form.Group as={Col}>
                                                <Form.Label>harlienails@gmail.com</Form.Label>
                                            </Form.Group>
                                        </Form.Row>
                                        <Form.Row>
                                            <Form.Group as={Col}>
                                                <Form.Label style={{ fontWeight: 'bold' }}>Phone Number:</Form.Label>
                                            </Form.Group>
                                            <Form.Group as={Col}>
                                                <Form.Label>(515) 555 5555</Form.Label>
                                            </Form.Group>
                                        </Form.Row>

                                        <Form.Label style={{ fontWeight: 'bold' }}>Business Hours:</Form.Label>
                                        <Form.Row>
                                            <Form.Group as={Col}>
                                                <Form.Label>Monday: 9:00am - 8:00pm</Form.Label>
                                                <Form.Label>Wednesday: 9:00am - 8:00pm</Form.Label>
                                                <Form.Label>Friday: 9:00am - 8:00pm</Form.Label>
                                                <Form.Label>Sunday: 11:00am - 5:00pm</Form.Label>
                                            </Form.Group>
                                            <Form.Group as={Col}>
                                                <Form.Label>Tuesday: 9:00am - 8:00pm</Form.Label>
                                                <Form.Label>Thursday: 9:00am - 8:00pm</Form.Label>
                                                <Form.Label>Saturday: 9:00am - 8:00pm</Form.Label>
                                            </Form.Group>

                                        </Form.Row>
                                    </Form>
                                </Card.Body>
                            </Col>
                        </Row>
                    </Card>
                </Container>
            </section>


            <section>
                <Container>
                    <Row>
                        <Col>
                            <h2 className="text-center">Location</h2>
                        </Col>
                    </Row>

                </Container>
            </section>

        </div>
    )
}
