import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { createStaffData } from '../../../api/data/staffs-data';
import { StaffCardGroupComponent } from '../../global/StaffCardGroupComponent/StaffCardGroupComponent';

export default class AboutComponent extends React.Component {
    render() {
        return (
            <div>
                <h1> About Us</h1>
                <section>
                    <Container>
                        <Row>
                            <Col>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </Col>
                        </Row>
                    </Container>
                </section>

                <section>
                    <Container>
                        <Row>
                            <Col>
                                <h2 style={{ textAlign: 'center' }}>Our Staffs</h2>
                                <StaffCardGroupComponent staffData={createStaffData()} />

                            </Col>
                        </Row>
                    </Container>
                </section>
            </div>
        );
    }
}