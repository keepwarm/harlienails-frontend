import React from 'react';
import { BannerComponent } from '../../global/BannerComponent/BannerComponent';
import { Row, Container, Col, Form, Card, Image, CardGroup } from 'react-bootstrap';
import { ServicesComponent } from '../../global/ServicesComponent/ServicesComponent';
// import { AdvicesComponent } from '../../global-components/UsingFreeAPI/AdvicesComponent/AdvicesComponent';
// import { QuotesComponent } from '../../global-components/UsingFreeAPI/QuotesComponent/QuotesComponent';
import { Coupons } from '../../global/Coupons/Coupons';
import { getServicesData } from '../../../api/data/service-data';
import { Gallery } from '../../global/Gallery/Gallery';

export default class HomeComponent extends React.Component {
    constructor(props) {
        super(props);
        this.createBannerData = this.createBannerData.bind(this);
    }

    createBannerData() {
        return [
            {
                imageUrl: 'https://tnailsspahouston.com/uploads/demo23uxpnnim/logo/2017/07/17/1_1500302242_95_33.jpg',
                title: 'Nails French',
                // description: 'Nails Art 1'
            },
            {
                imageUrl: 'https://queennailscharlottetown.com/uploads/fnail0afw5skl/logo/2019/07/11/s9.png',
                title: 'Ombre Design',
                // description: 'Nails Art 2'
            },
            {
                imageUrl: 'https://cknailsspa.com/uploads/fdemoicpswuke/logo/2018/06/14/2_1529029226_0_Slide%20(8).jpg',
                title: 'Hot Pink',
                // description: 'Nails Art 3'
            }
        ];

    }

    render() {
        console.log(this.props);
        return (
            <div>
                <section>
                    <BannerComponent bannerData={this.createBannerData()} />
                </section>
                <section>
                    <Container>
                        <Row>
                            <Col>
                                <h2 className="text-center">Our Services</h2>
                                <CardGroup>
                                    {getServicesData().map((data) => (
                                        <ServicesComponent header={data.header} image={data.image} title={data.title} content={data.content} />
                                    ))}
                                </CardGroup>
                            </Col>
                        </Row>
                    </Container>
                </section>

                <section>
                    <Container>
                        <Row>
                            <Col>
                                <Coupons />
                            </Col>
                        </Row>
                    </Container>
                </section>

                <section>
                    <Container>
                        <Row>
                            <Col>
                                <h2 style={{ textAlign: 'center' }}>Our Gallery</h2>
                                <Gallery />
                            </Col>
                        </Row>
                    </Container>
                </section>
                
                <Container>
                    <Row>
                        <Col>
                            {/* <AdvicesComponent /> */}
                        </Col>
                    </Row>
                </Container>
                <Container>
                    <Row>
                        <Col>
                            {/* <QuotesComponent /> */}
                        </Col>
                    </Row>
                </Container>
            </div>

        );
    }
}
