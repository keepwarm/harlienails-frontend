import Axios from 'axios'
import React, { useEffect, useState } from 'react'
import PricingMenu from '../../../global/PricingMenu/PricingMenu'
import ServiceItem from '../../../global/ServiceItem/ServiceItem'
import additionImage from '../../../../media/addition1.jpg';
import waxingImage from '../../../../media/waxing1.jpg';
import { Row, Col, Container } from 'react-bootstrap';

export default function OtherServices() {
    const [services, setservices] = useState([])
    useEffect(() => {
        const getServices = async() => {
            const response = await Axios.get("http://localhost:3090/services")
            setservices(response.data);
        }
        getServices();
    }, [])

    const renderAdditionalServices = () => {
        const additionalServices = services.filter(service => service.Category === "Addition");
        console.log(additionalServices);
        console.log(services)
        return additionalServices.map((service) => {
            return (
                <Col md={6} xs={12}>
                    <ServiceItem title={service.Name}
                                 price={service.Price}
                                 content={service.Description}
                    />
                </Col>
            )
        })
    }

    const renderWaxingServices = () => {
        const waxingServices = services.filter(service => service.Category === "Waxing");
        return waxingServices.map((service) => {
            return (
                <Col md={6} xs={12}>
                    <ServiceItem title={service.Name}
                                 price={service.Price}
                                 content={service.Description}
                    />
                </Col>
            )
        })
    }
    return (
        <div>
            <h1>Other Services</h1>
            <Container>
                <Row>
                    <Col>
                        <h3>Additional Services</h3>
                    </Col>
                </Row>
            </Container>
            <PricingMenu image={additionImage}>
                <Container>
                    <Row>
                        {renderAdditionalServices()}
                    </Row>
                </Container>
            </PricingMenu>
            <hr />
            <Container>
                <Row>
                    <Col>
                        <h3>Waxing Services</h3>
                    </Col>
                </Row>
            </Container>
            <PricingMenu image={waxingImage}>
                <Container>
                    <Row>
                        {renderWaxingServices()}
                    </Row>
                </Container>
            </PricingMenu>
        </div>
    )
}
