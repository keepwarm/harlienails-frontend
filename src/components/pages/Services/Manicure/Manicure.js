import React, {useEffect, useState} from 'react'
import { Row, Col, Container } from 'react-bootstrap'
import PricingMenu from '../../../global/PricingMenu/PricingMenu'
import ServiceItem from '../../../global/ServiceItem/ServiceItem'
import image from '../../../../media/gallery2.jpg';
import Axios from 'axios';

export default function Manicure() {
    const [services, setservices] = useState([])
    useEffect(() => {
        const getServices = async() => {
            const response = await Axios.get("http://localhost:3090/services")
            console.log(response);
            setservices(response.data);
        }
        getServices();
    }, [])
    const renderServices = () => {
        const manicure = services.filter(service => service.Category === "Manicure")
        return manicure.map((service) => {
            return (
                <Col md={6} xs={12}>
                <ServiceItem title={service.Name}
                             price={service.Price}
                             content={service.Description}
                />
                </Col>
            );
        })
    }

    return (
        <div>
            <h1>Manicure Services</h1>
            <PricingMenu image={image}>
                <Container>
                    <Row>
                        {renderServices()}
                        {/* <Col md={6} xs={12}>
                            <div>
                            <h6>Basic Manicure <span>$15</span></h6>             
                            <p>Includes a warm hand soak, nail shaping, cuticle care, buffing, and a relaxing hang massage topped off with polish of your choice.</p>
                        </div> 
                            <ServiceItem title="Basic Manicure"
                                price={15}
                                content="Includes a warm hand soak, nail shaping, cuticle care, buffing, and a relaxing hand massage topped off with polish of your choice."
                            />
                        </Col>
                        <Col md={6} xs={12}>
                            <ServiceItem title="French Manicure"
                                price={20}
                                content="Includes a warm hand soak, nail shaping, cuticle care, buffing, and a relaxing hand massage topped off with French Tip of your choice."
                            />
                        </Col>
                        <Col md={6} xs={12}>

                            <ServiceItem title="Gel Manicure"
                                price={30}
                                content="Includes a warm hand soak, nail shaping, cuticle care, buffing, and a relaxing hand massage topped off with Gel polish of your choice using the special Gel machine."
                            />
                        </Col>
                        <Col md={6} xs={12}>
                            <ServiceItem title="French Gel Manicure"
                                price={35}
                                content="Includes a warm hand soak, nail shaping, cuticle care, buffing, and a relaxing hand massage topped off with Gel French polish of your choice using the special Gel machine."
                            />
                        </Col>
                        <Col md={6} xs={12}>
                            <ServiceItem title="Special Manicure"
                                price={30}
                                content="Includes a warm hand soak, nail shaping, cuticle care, buffing, and a relaxing hand stone massage with speical lotion topped off with polish of your choice."
                            />
                        </Col> */}
                    </Row>
                </Container>
            </PricingMenu>
        </div>
    )
}
