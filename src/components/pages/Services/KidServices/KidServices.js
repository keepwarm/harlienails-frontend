import Axios from 'axios'
import React, { useEffect, useState } from 'react'
import ServiceItem from '../../../global/ServiceItem/ServiceItem'
import image from '../../../../media/kid1.jpg';
import PricingMenu from '../../../global/PricingMenu/PricingMenu';
import { Container } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
export default function KidServices() {
    const [services, setservices] = useState([])
    useEffect(()=> {
        const getServices = async() => {
            const response = await Axios.get("http://localhost:3090/services")
            setservices(response.data);
        }
        getServices();
    }, [])

    const renderServices = () => {
        const kidService = services.filter(service =>service.Category === "Kid")
        return kidService.map((service) => {
            return (
                <Col md={6} xs={12}>
                    <ServiceItem title={service.Name}
                                 price={service.Price}
                                 content={service.Description}
                    />
                </Col>
            )
        })
    }

    return (
        <div>
            <h1>Children 10 & Under</h1>
            <PricingMenu image={image}>
                <Container>
                    <Row>
                        {renderServices()}
                    </Row>
                </Container>
            </PricingMenu>
        </div>
    )
}
