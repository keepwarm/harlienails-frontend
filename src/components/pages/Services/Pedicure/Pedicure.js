import Axios from 'axios'
import React, { useEffect, useState } from 'react';
import { Row, Col } from 'react-bootstrap';
import { Container } from 'react-bootstrap';
import image from '../../../../media/pedicure1.jpg';
import ServiceItem from '../../../global/ServiceItem/ServiceItem';
import PricingMenu from '../../../global/PricingMenu/PricingMenu';


export default function Pedicure() {
    const [services, setservices] = useState([])
    useEffect(() => {
        const getServices = async() => {
            const response = await Axios.get("http://localhost:3090/services")
            setservices(response.data);
        }
        getServices();
    }, [])

    const renderServices = () => {
        const pedicure = services.filter(service => service.Category === "Pedicure")
        return pedicure.map((service) => {
            return (
                <Col md={6} xs={12}>
                    <ServiceItem title={service.Name}
                                 price={service.Price}
                                 content={service.Description}
                    />
                </Col>
            )
        })
    } 

    return (
        <div>
            <h1>Pedicure Spa</h1>
            <PricingMenu image={image}>
                <Container>
                    <Row>
                        {renderServices()}
                    </Row>
                </Container>
            </PricingMenu>
        </div>
    )
}
