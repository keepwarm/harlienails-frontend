import React from 'react';
import HomeComponent from '../pages/Home/HomeComponent';
import AboutComponent from '../pages/About/AboutComponent';
import Pedicure from '../pages/Services/Pedicure/Pedicure';
import Manicure from '../pages/Services/Manicure/Manicure';
import OtherServices from '../pages/Services/OtherServices/OtherServices';
import PowderNails from '../pages/Services/PowderNails/PowderNails';
import KidServices from '../pages/Services/KidServices/KidServices';
import Appointment from '../pages/Appointment/Appointment';
import Contacts from '../pages/Contacts/Contacts';
import {
    Switch,
    Route
  } from 'react-router-dom';


export default function RouterComponent() {
    return (
        <Switch>
            <Route exact path='/'>
                <HomeComponent />
            </Route>
            <Route exact path='/about'>
                <AboutComponent />
            </Route>
            <Route exact path='/ped'>
                <Pedicure />
            </Route>
            <Route exact path='/mani'>
                <Manicure />
            </Route>
            <Route exact path='/other'>
                <OtherServices />
            </Route>
            <Route exact path='/powder'>
                <PowderNails />
            </Route>
            <Route exact path='/kid'>
                <KidServices />
            </Route>
            <Route exact path='/appointment'>
                <Appointment />
            </Route>
            <Route exact path='/contact'>
                <Contacts />
            </Route>
        </Switch> 
    )
}
