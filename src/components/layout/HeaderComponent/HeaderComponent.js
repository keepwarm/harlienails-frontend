import React from 'react';
import { Navbar, NavDropdown, Col, Nav, Container, Row, Modal } from 'react-bootstrap';
import logo from '../../../media/HarlieNailsLogo.png';
import { NavLink, Link } from 'react-router-dom';
import './HeaderComponent.css';

export class HeaderComponent extends React.Component {
    render() {
        return (
            <header>
                <Navbar expand="lg">
                    <Navbar.Brand className="nav-item d-inline d-lg-none">
                        <Link class="nav-link" to='/'>
                            <img src={logo} height='80' width='120' alt='Harlie Nails' />
                        </Link>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Link className='nav-item' as={NavLink} exact to='/' activeClassName="active">Home</Nav.Link>
                            <Nav.Link className='nav-item' as={NavLink} exact to='/about' activeClassName="active">About</Nav.Link>
                            <NavDropdown className="nav-item" activeClassName="active" title='Services' id='basic-nav-dropdown'>
                                <NavDropdown.Item as={NavLink} exact activeClassName="active" to='/mani'>Manicure</NavDropdown.Item>
                                <NavDropdown.Item as={NavLink} exact activeClassName="active" to='/ped'>Pedicure</NavDropdown.Item>
                                <NavDropdown.Item as={NavLink} exact activeClassName="active" to='/powder'>Powder Nails</NavDropdown.Item>
                                <NavDropdown.Item as={NavLink} exact activeClassName="active" to='/other'>Other Services</NavDropdown.Item>
                                <NavDropdown.Item as={NavLink} exact activeClassName="active" to='/kid'>Kids Services</NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                        <Navbar.Brand className="mx-auto d-none d-lg-inline">
                            <Link class="nav-link" to='/'>
                                <img src={logo} height='80' width='120' alt='Harlie Nails' />
                            </Link>
                        </Navbar.Brand>
                        <Nav className="ml-auto">
                            <NavDropdown className='nav-item' title='Products' activeClassName="active" id='basic-nav-dropdown'>
                                <NavDropdown.Item as={NavLink} exact activeClassName="active" to='/dip'>Dip Powder</NavDropdown.Item>
                                <NavDropdown.Item as={NavLink} exact activeClassName="active" to='/gel'>Gel Polish</NavDropdown.Item>
                                <NavDropdown.Item as={NavLink} exact activeClassName="active" to='/regular'>Regular Polish</NavDropdown.Item>
                            </NavDropdown>
                            <Nav.Link className="nav-item" as={NavLink} exact activeClassName="active" to='/appointment'>Appointment</Nav.Link>
                            <Nav.Link className="nav-item" as={NavLink} exact activeClassName="active" to='/contact'>Contacts</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </header>
        )
    }
};