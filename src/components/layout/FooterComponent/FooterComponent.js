import React from 'react';
import {Row, Col, Container, ListGroup, Card } from 'react-bootstrap';
import './FooterComponent.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export class FooterComponent extends React.Component {
    render() {
        return (
            <footer>
                <Container>
                    <Row>
                        <Col>
                            <h3>Harlie Nails</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </Col>
                        <Col>
                            <h5>Products</h5>
                            <ul>
                                <li>
                                    <a href = ''>Nail Polishes</a>
                                </li>
                                <li>
                                    <a href = '' >Lotions</a>
                                </li>
                                <li>
                                    <a href = '' >Nail Clippers Set</a>
                                </li>
                                <li>
                                    <a href = '' >Others</a>
                                </li>
                            </ul>
                        </Col>
                        <Col>
                            <h5>Services</h5>
                            <ul>
                                <li>
                                    <a href = ''>Pedicures</a>
                                </li>
                                <li>
                                    <a href = '' >Manicures</a>
                                </li>
                                <li>
                                    <a href = '' >Fullset</a>
                                </li>
                                <li>
                                    <a href = '' >Others</a>
                                </li>
                            </ul>
                        </Col>
                    
                    </Row>
                    <hr className = 'm-0'/>
                    
                    <Row>
                        <Col>
                            <div className = 'socialIcons'>
                                <a className = 'facebook' href="#"><FontAwesomeIcon icon = {['fab', 'facebook']}/></a>
                                <a className = 'twitter' href="#"><FontAwesomeIcon icon = {['fab', 'twitter']}/></a>
                                <a className = 'instagram' href="#"><FontAwesomeIcon icon = {['fab', 'instagram']}/></a>
                                <a className = 'linkedin' href="#"><FontAwesomeIcon icon = {['fab', 'linkedin']}/></a>
                                <a className = 'skype' href="#"><FontAwesomeIcon icon = {['fab', 'skype']}/></a>
                            </div>
                        </Col>
                    </Row>
                </Container>

                <Container fluid style={{ paddingLeft: 0, paddingRight: 0 }}>
                    <Row noGutters = {true}>
                        <Col>
                            <div class="footer-copyright text-center py-3">
                            Copyright © 2020 : Harlie Nails
                            </div>
                        </Col>
                    </Row>
                </Container>
            </footer>
        );
    }
};